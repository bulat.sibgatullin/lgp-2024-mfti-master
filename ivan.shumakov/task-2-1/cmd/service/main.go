package main

import (
	"errors"
	"fmt"
)

// interval [,].
type Interval struct {
	From uint
	To   uint
}

func (interval *Interval) AddConstraint(constraint string, t uint) error {
	if constraint == ">=" && t >= interval.From {
		interval.From = t
		return nil
	}
	if constraint == "<=" && t <= interval.To {
		interval.To = t
		return nil
	}
	if constraint != "<=" && constraint != ">=" {
		return errors.New("invalid constraint:" + constraint)
	}
	return nil
}

func (interval Interval) GetOptimal() int {
	if interval.From <= interval.To {
		return int(interval.From)
	}
	return -1
}

func checkInputErr(err error) {
	if err != nil {
		panic("invalid input")
	}
}

func parseDepartment() {
	var numOfEmployees uint
	_, err := fmt.Scanf("%d", &numOfEmployees)
	checkInputErr(err)

	interv := Interval{From: 15, To: 30}
	for i := uint(0); i < numOfEmployees; i++ {
		var constraint string
		_, err = fmt.Scanf("%s", &constraint)
		checkInputErr(err)

		var t uint
		_, err = fmt.Scanf("%d", &t)
		checkInputErr(err)

		err = interv.AddConstraint(constraint, t)
		checkInputErr(err)

		fmt.Println(interv.GetOptimal())
	}
}

func main() {
	var numOfDepartments uint
	_, err := fmt.Scanf("%d", &numOfDepartments)
	checkInputErr(err)

	for i := uint(0); i < numOfDepartments; i++ {
		parseDepartment()
		fmt.Println()
	}
}
