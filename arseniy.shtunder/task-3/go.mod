module gitlab.com/arseniy.shtunder/task-3

go 1.21.0

require (
	golang.org/x/text v0.14.0
	gopkg.in/yaml.v3 v3.0.1
)
