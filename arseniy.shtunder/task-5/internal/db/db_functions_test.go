package db_test

import (
	"errors"
	"testing"

	"github.com/DATA-DOG/go-sqlmock"
	"github.com/stretchr/testify/require"
	"gitlab.com/arseniy.shtunder/task-5/internal/db"
)

type rowTestDb struct {
	names       []string
	errExpected error
}

var testTable = []rowTestDb{
	{
		names: []string{"Ivan, Gena228"},
	},
	{
		names:       nil,
		errExpected: errors.New("NameError"),
	},
}

type valueTestDb struct {
	values      []string
	errExpected error
}

var testValueTable = []valueTestDb{
	{
		values: []string{"here123@google.com", "baby@yandex.ru"},
	},
	{
		values:      nil,
		errExpected: errors.New("ValueError"),
	},
}

func mockDbRows(names []string) *sqlmock.Rows {
	rows := sqlmock.NewRows([]string{"name"})

	for _, name := range names {
		rows = rows.AddRow(name)
	}

	return rows
}

func TestGetName(t *testing.T) {
	mockDB, mock, err := sqlmock.New()
	if err != nil {
		t.Fatalf("an error '%s' was not expected when marshaling expected json data", err)
	}

	dbService := db.New(mockDB)

	for i, row := range testTable {
		mock.ExpectQuery("SELECT name FROM users").WillReturnRows(mockDbRows(row.names)).WillReturnError(row.errExpected)
		names, err := dbService.GetNames()

		if row.errExpected != nil {
			require.ErrorIs(t, err, row.errExpected, "row: %d, expected error: %w, actual error: %w", i, row.errExpected, err)
			require.Nil(t, names, "row: %d, names must be nil", i)
			continue
		}

		require.NoError(t, err, "row: %d, error must be nil", i)
		require.Equal(t, row.names, names, "row: %d, expected names: %s, actual names: %s", i, row.names, names)
	}
}

func TestSelectUniqueValues(t *testing.T) {
	mockDB, mock, err := sqlmock.New()
	if err != nil {
		t.Fatalf("an error '%s' was not expected when marshaling expected json data", err)
	}

	dbService := db.New(mockDB)

	columnName := "email"
	tableName := "users"
	query := "SELECT DISTINCT " + columnName + " FROM " + tableName

	for i, row := range testValueTable {
		mock.ExpectQuery(query).WillReturnRows(mockDbRows(row.values)).WillReturnError(row.errExpected)
		values, err := dbService.SelectUniqueValues(columnName, tableName)

		if row.errExpected != nil {
			require.ErrorIs(t, err, row.errExpected, "row: %d, expected error: %w, actual error: %w", i, row.errExpected, err)
			require.Nil(t, values, "row: %d, values must be nil", i)
			continue
		}

		require.NoError(t, err, "row: %d, error must be nil", i)
		require.Equal(t, row.values, values, "row: %d, expected values: %s, actual values: %s", i, row.values, values)
	}
}

func TestGetNameError(t *testing.T) {
	mockDB, mock, err := sqlmock.New()
	if err != nil {
		t.Fatalf("an error '%s' was not expected when marshaling expected json data", err)
	}

	dbService := db.New(mockDB)

	rows := sqlmock.NewRows([]string{"name"}).AddRow("first").AddRow("second").RowError(1, errors.New("RowError"))
	mock.ExpectQuery("SELECT name FROM users").WillReturnRows(rows)

	names, err := dbService.GetNames()

	require.Error(t, err, "error: %w", err)
	require.Nil(t, names, "names must be nil")

	if err := mock.ExpectationsWereMet(); err != nil {
		t.Errorf("there were unfulfilled expectations: %s", err)
	}
}

func TestSelectUniqueValuesError(t *testing.T) {
	mockDB, mock, err := sqlmock.New()
	if err != nil {
		t.Fatalf("an error '%s' was not expected when marshaling expected json data", err)
	}

	dbService := db.New(mockDB)

	columnName := "email"
	tableName := "users"
	query := "SELECT DISTINCT " + columnName + " FROM " + tableName

	rows := sqlmock.NewRows([]string{"name"}).AddRow("first").AddRow("second").RowError(1, errors.New("RowError"))
	mock.ExpectQuery(query).WillReturnRows(rows)

	values, err := dbService.SelectUniqueValues(columnName, tableName)

	require.Error(t, err, "error: %w", err)
	require.Nil(t, values, "values must be nil")

	if err := mock.ExpectationsWereMet(); err != nil {
		t.Errorf("there were unfulfilled expectations: %s", err)
	}
}
