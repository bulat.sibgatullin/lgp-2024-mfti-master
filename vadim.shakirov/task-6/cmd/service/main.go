package main

import (
	"errors"
	"fmt"
	"sync"
)

type Node struct {
	Data int
	Prev *Node
	Next *Node
}

type List struct {
	head  *Node
	tail  *Node
	size  int
	mutex sync.Mutex
}

func (l *List) PushBack(data int) error {
	newNode := &Node{Data: data, Next: nil, Prev: nil}

	if l.tail == nil {
		l.tail = newNode
		l.head = newNode
		l.size++
	} else {
		l.tail.Next = newNode
		newNode.Prev = l.tail
		l.size++

		l.tail = newNode
	}

	return nil
}

func (l *List) PopFront() (int, error) {
	if l.size == 0 {
		return 0, errors.New("Try to pop from empty list")
	}

	retData := l.head.Data

	if l.head.Next == nil {
		l.head = nil
		l.tail = nil
	} else {
		l.head = l.head.Next
		l.head.Prev = nil
	}
	l.size--

	return retData, nil
}

func (l *List) Size() int {
	return l.size
}

func (l *List) Dump() {
	fmt.Println("Size is", l.size)
	it := l.head
	for {
		if it != nil {
			fmt.Println(it.Data, ":", it.Prev, " - ", it.Next)
			it = it.Next
		} else {
			break
		}
	}
}

func NewList() *List {
	newList := &List{head: nil, tail: nil, size: 0}

	return newList
}

func (l *List) PushBackSafe(data int) error {
	newNode := &Node{Data: data, Next: nil, Prev: nil}

	l.mutex.Lock()
	if l.tail == nil {
		l.tail = newNode
		l.head = newNode
		l.size++
	} else {
		l.tail.Next = newNode
		newNode.Prev = l.tail
		l.size++

		l.tail = newNode
	}
	l.mutex.Unlock()

	return nil
}

func (l *List) PopFrontSafe() (int, error) {
	l.mutex.Lock()
	if l.size == 0 {
		return 0, errors.New("Try to pop from empty list")
	}

	retData := l.head.Data

	if l.head.Next == nil {
		l.head = nil
		l.tail = nil
	} else {
		l.head = l.head.Next
		l.head.Prev = nil
	}
	l.size--
	l.mutex.Unlock()

	return retData, nil
}

func (l *List) SizeSafe() int {
	l.mutex.Lock()
	size := l.size
	l.mutex.Unlock()
	return size
}

func UnsafeQueue(numGoroutines int) {
	queue := NewList()

	var wg sync.WaitGroup

	pushElements := func(queue *List, num int) {
		defer wg.Done()
		for i := 0; i < 1000000; i++ {
			err := queue.PushBack(num)
			if err != nil {
				fmt.Println("We don't expect errors")
			}
		}
	}

	for i := 1; i <= numGoroutines; i++ {
		wg.Add(1)
		go pushElements(queue, i)
	}
	wg.Wait()

	fmt.Println("Unsafe sum queue size:", queue.Size())
}
func SafeQueue(numGoroutines int) {
	queue := NewList()

	var wg sync.WaitGroup

	pushElements := func(queue *List, num int) {
		defer wg.Done()
		for i := 0; i < 1000000; i++ {
			err := queue.PushBackSafe(num)
			if err != nil {
				fmt.Println("We don't expect errors")
			}
		}
	}

	for i := 1; i <= numGoroutines; i++ {
		wg.Add(1)
		go pushElements(queue, i)
	}
	wg.Wait()

	fmt.Println("Safe   sum queue size:", queue.SizeSafe())
}

func main() {
	numGoroutines := 4
	UnsafeQueue(numGoroutines)
	SafeQueue(numGoroutines)
}
