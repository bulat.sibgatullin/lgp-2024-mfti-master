package main

import (
	"fmt"
	"sync"
	"sync/atomic"
	"unsafe"
)

type node struct {
	next unsafe.Pointer
	v    interface{}
}

type MyStack struct {
	top unsafe.Pointer
	len uint64
}

func NewStack() *MyStack {
	return &MyStack{}
}

func (s *MyStack) PopSafe() interface{} {
	var top, next unsafe.Pointer
	var newNode *node
	for {
		top = atomic.LoadPointer(&s.top)
		if top == nil {
			return nil
		}
		newNode = (*node)(top)
		next = atomic.LoadPointer(&newNode.next)
		if atomic.CompareAndSwapPointer(&s.top, top, next) {
			atomic.AddUint64(&s.len, ^uint64(0))
			return newNode.v
		}
	}
}

func (s *MyStack) PushSafe(v interface{}) {
	newNode := node{v: v}
	var top unsafe.Pointer
	for {
		top = atomic.LoadPointer(&s.top)
		newNode.next = top
		if atomic.CompareAndSwapPointer(&s.top, top, unsafe.Pointer(&newNode)) {
			atomic.AddUint64(&s.len, 1)
			return
		}
	}
}

func (s *MyStack) Pop() interface{} {
	var top, next unsafe.Pointer
	var newNode *node

	top = s.top
	if top == nil {
		return nil
	}
	newNode = (*node)(top)
	next = newNode.next
	s.len--
	s.top = next
	return newNode.v
}

func (s *MyStack) Push(v interface{}) {
	newNode := node{v: v}
	top := s.top
	newNode.next = top
	s.len++
	s.top = unsafe.Pointer(&newNode)
}

func main() {
	s := NewStack()

	var wg sync.WaitGroup

	for i := 0; i < 8; i++ {
		wg.Add(1)
		go func(i int) {
			defer wg.Done()
			for j := 0; j < 100000; j++ {
				s.PushSafe(i)
			}
		}(i)
	}

	wg.Wait()

	for i := 0; i < 8; i++ {
		wg.Add(1)
		go func() {
			defer wg.Done()
			for j := 0; j < 100000; j++ {
				s.PopSafe()
			}
		}()
	}

	wg.Wait()

	fmt.Println(s.len)

	for i := 0; i < 8; i++ {
		wg.Add(1)
		go func(i int) {
			defer wg.Done()
			for j := 0; j < 100000; j++ {
				s.Push(i)
			}
		}(i)
	}

	wg.Wait()

	for i := 0; i < 8; i++ {
		wg.Add(1)
		go func() {
			defer wg.Done()
			for j := 0; j < 100000; j++ {
				s.Pop()
			}
		}()
	}

	wg.Wait()

	fmt.Println(s.len)
}
