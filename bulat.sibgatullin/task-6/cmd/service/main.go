package main

import (
	"fmt"
	"sync"
)

type num struct {
	myNum int
	mutex sync.Mutex
}

func addUnsafe(numGoroutines int) {
	addChecker := &num{myNum: 0}

	var wg sync.WaitGroup

	addElement := func(addChecker *num) {
		defer wg.Done()
		for i := 0; i < 100000; i++ {
			addChecker.myNum++
		}
	}

	for i := 1; i <= numGoroutines; i++ {
		wg.Add(1)
		go addElement(addChecker)
	}
	wg.Wait()

	fmt.Println("num:", addChecker.myNum)
}

func addSafe(numGoroutines int) {
	addChecker := &num{myNum: 0}

	var wg sync.WaitGroup

	addElementSafe := func(addChecker *num) {
		defer wg.Done()
		for i := 0; i < 100000; i++ {
			addChecker.mutex.Lock()
			addChecker.myNum++
			addChecker.mutex.Unlock()
		}
	}

	for i := 1; i <= numGoroutines; i++ {
		wg.Add(1)
		go addElementSafe(addChecker)
	}
	wg.Wait()

	fmt.Println("num:", addChecker.myNum)
}

func main() {
	numGoroutines := 4
	addUnsafe(numGoroutines)
	addSafe(numGoroutines)
}
